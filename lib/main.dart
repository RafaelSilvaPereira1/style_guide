import 'package:flutter/material.dart';
import 'package:style_guide/export.dart';
import 'package:style_guide/src/screen_data.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  ScreenData sd;

  @override
  Widget build(BuildContext context) {

    sd = ScreenData.init(context);

    print(sd.dim(DimensionData(value: 10, type: 'h')));

    return Container(
      
    );
  }
}