import '../i_style_query.exception.dart';

/// [IStyleQuerySyntaxException] this abstract class represents an abstract exception for [StyleGuide]'s queries,
/// specifically, the syntax errors.
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 04/11/2020
/// 
abstract class IStyleQuerySyntaxException extends IStyleQueryException {

  /// [String] query: represents the query used to get a value from the [StyleGuide];
  final String query;

  const IStyleQuerySyntaxException({
    this.query,
  });

  @override
  String toString() {
    return super.toString();
  }
}
