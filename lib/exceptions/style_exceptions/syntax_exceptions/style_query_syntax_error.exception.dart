import 'i_style_query_syntax.exception.dart';

/// [StyleQuerySyntaxErrorException] this class represents an exception for [StyleGuide]'s queries,
/// specifically, for syntax errors on the query usage.
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 04/11/2020
/// 
class StyleQuerySyntaxErrorException extends IStyleQuerySyntaxException {
  
  StyleQuerySyntaxErrorException(String query) : super (
    query: query
  );

  @override
  String toString() {
    return "${super.toString()}\nSyntax error on query:\n'${super.query}'\n\nTry using something like:\n'group > style : property' (for Style Guide)\n\nOr just:\n'style : property' (for Style Set)\n";
  }
}