import 'i_style_query_syntax.exception.dart';

/// [InvalidCharactersException] this class represents an exception for [StyleGuide]'s queries,
/// specifically, for invalid characters.
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 04/11/2020
/// 
class InvalidCharactersException extends IStyleQuerySyntaxException {
  
  InvalidCharactersException(String query) : super (
    query: query
  );

  @override
  String toString() {
    return "${super.toString()}\nInvalid characters in query:\n'${super.query}'\n\nUse only:\n- [a-z];\n- [A-Z];\n- [0-9];\n- '-' (dash);\n- '_' (underscore);\n- '>' (greater than);\n- ':' (colon);\n- ' ' (whitespace).\n";
  }
}
