import 'i_element_not_found.exception.dart';

/// [GroupNotFoundException] this class represents an exception for [StyleGuide]'s queries,
/// specifically, for not found [Group].
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 04/11/2020
/// 
class GroupNotFoundException extends IElementNotFoundException {

  GroupNotFoundException(String groupKey, List<String> groups) : super(
    elementsList: groups,
    elementKey: groupKey,
    elementName: "Group",
    listName: "groups",
  );

  @override
  String toString() {
    return super.toString();
  }
}
