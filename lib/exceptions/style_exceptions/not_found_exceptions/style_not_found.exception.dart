import 'i_element_not_found.exception.dart';

/// [StyleNotFoundException] this class represents an exception for [StyleGuide]'s queries,
/// specifically, for not found [Style].
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 04/11/2020
/// 
class StyleNotFoundException extends IElementNotFoundException {

  StyleNotFoundException(String styleKey, List<String> styles) : super(
    elementsList: styles,
    elementKey: styleKey,
    elementName: "Style",
    listName: "styles",
  );

  @override
  String toString() {
    return super.toString();
  }
} 
