import '../i_style_query.exception.dart';

/// [IElementNotFoundException] this abstract class represents an abstract exception for [StyleGuide]'s queries,
/// specifically, the 'not found' errors.
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 04/11/2020
/// 
abstract class IElementNotFoundException extends IStyleQueryException {

  /// [String] elementKey: represents the name of the element, the [StyleGuide] access key;
  final String elementKey;

  /// [String] elementName: represents the name of the type of the element, which can be a [Property],
  /// a [Style] or a [Group];
  final String elementName;

  /// [List<String>] elementList: represents the list of elements;
  final List<String> elementsList;

  /// [String] listName: the name of the list of elements, equivalent to the plural of the [elementName];
  final String listName;

  const IElementNotFoundException({
    this.elementsList,
    this.elementKey,
    this.listName,
    this.elementName,
  });

  /// Create a [String] with the elements listed in sequence.
  ///
  elementsListToString(List<String> eList) {
    String result = "";
    eList.forEach((e) { 
      result += "- ${e.toString()};\n";
    });

    return result;
  }

    @override
  String toString() {
    String list = elementsListToString(elementsList);
    return "${super.toString()}\n\n$elementName '$elementKey' not found.\n\nThe available $listName are:\n$list\n";
  }
}
