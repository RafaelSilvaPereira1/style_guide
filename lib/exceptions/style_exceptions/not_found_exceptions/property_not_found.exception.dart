import 'i_element_not_found.exception.dart';

/// [PropertyNotFoundException] this class represents an exception for [StyleGuide]'s queries,
/// specifically, for not found [Property].
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 04/11/2020
/// 
class PropertyNotFoundException extends IElementNotFoundException {

  PropertyNotFoundException(String propertyKey, List<String> properties, String styleKey) : super(
    elementsList: properties,
    elementKey: propertyKey,
    elementName: "Property",
    listName: "properties in '$styleKey'",
  );

  @override
  String toString() {
    return super.toString();
  }
} 
