/// [IStyleQueryException] this abstract class represents an abstract exception for [StyleGuide]'s queries.
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 04/11/2020
/// 
abstract class IStyleQueryException implements Exception {

  const IStyleQueryException();

  @override
  String toString() {
    return "Error in style query!";
  }
}