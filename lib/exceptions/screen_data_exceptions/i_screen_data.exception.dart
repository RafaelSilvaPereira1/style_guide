/// [IScreenDataException] this abstract class represents an abstract exception for [ScreenData].
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 06/11/2020
/// 
abstract class IScreenDataException implements Exception {

  const IScreenDataException();

  @override
  String toString() {
    return "Error using ScreenData!";
  }
}