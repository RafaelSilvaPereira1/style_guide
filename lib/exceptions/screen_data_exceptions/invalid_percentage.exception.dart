import 'i_screen_data.exception.dart';

/// [InvalidPercentageException] this class represents an exception for [ScreenData],
/// specifically for errors with invalid percentages.
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 06/11/2020
/// 
class InvalidPercentageException extends IScreenDataException {
  
  final double percentage;

  InvalidPercentageException({this.percentage});

  @override
  String toString() {
    return "${super.toString()}\n\nInvalid percentage:\n'$percentage%'\n\nPercentage can't be less than zero.\n";
  }
}