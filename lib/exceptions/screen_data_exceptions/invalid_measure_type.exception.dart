import 'i_screen_data.exception.dart';

/// [InvalidMeasureTypeException] this class represents an exception for [ScreenData],
/// specifically for errors with the measure type.
///
/// @created_by: Rafael S. Pereira
///
/// @created_by: Marcelo F. S. Andrade
/// 
/// @at: 06/11/2020
/// 
class InvalidMeasureTypeException extends IScreenDataException {
  
  final String type;

  InvalidMeasureTypeException({this.type});

  @override
  String toString() {
    print(type);
    return "${super.toString()}\n\nInvalid measure type:\n'$type'\n\nUse only:\n- 'w' (for Width);\n- 'h' (for Height).\n";
  }
}