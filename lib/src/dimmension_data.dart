/// dd method is a wrap function to create a DimensionData
DimensionData dd(double value, String type) {
  return DimensionData(
    type: type,
    value: value,
  );
}

/// [DimensionData] is a bearer class to queries used in ScreenData.dim()
class DimensionData {

  /// [String] type: refer's to height **'h'** or **'w'** width
  /// valid terms:
  ///   - 'w',
  ///   - 'h'
  final String type;
  final double value;

  const DimensionData({this.type, this.value});

  String toString() => '$value$type';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is DimensionData && o.type == type && o.value == value;
  }

  @override
  int get hashCode => type.hashCode ^ value.hashCode;
}
