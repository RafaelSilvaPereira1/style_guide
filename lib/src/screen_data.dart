import 'package:flutter/material.dart';
import '../exceptions/screen_data_exceptions/invalid_measure_type.exception.dart';
import '../exceptions/screen_data_exceptions/invalid_percentage.exception.dart';

import 'dimmension_data.dart';

/// [ScreenData] this class represents the dimensions of the screen
///
/// @created_by: Rafael S Pereira
///
/// @created_by: Marcelo F. S Andrade
/// 
/// @at: 15/10/2020
class ScreenData {

  /// [Map<DimensionData, double>] _cachedHeights: this map represents a cache of
  /// already calculated height values using [dim] method;
  final Map<DimensionData, double> _cachedHeights;

  /// [Map<DimensionData, double>] _cachedWidths: this map represents a cache of
  /// already calculated width values using [dim] method;
  final Map<DimensionData, double> _cachedWidths;

  /// [double] _screenHeight: represents the total height of the screen in pixels;
  final double _screenHeight;

  /// [double] _screenWidth: represents the total width of the screen in pixels;
  final double _screenWidth;

  const ScreenData(this._screenHeight, this._screenWidth, this._cachedWidths, this._cachedHeights);

  /// creates a new ScreenData from a given Context
  factory ScreenData.init(BuildContext context) {

    final size = MediaQuery.of(context).size;
    final height = size.height;
    final width = size.width;
    // screenData.init(context);
    final ScreenData screenData = ScreenData(height, width, {}, {});

    return screenData;
  }

  /// [dim] get a [double] relative  dimension based  on a screen percent's
  double dim(DimensionData dimensionData) {

    final key = dimensionData;
    final value = dimensionData.value;
    final type = dimensionData.type.toLowerCase().replaceAll(RegExp(r"\s+"),"");

    _verifyDDValue(value);
    _verifyDDType(type);

    double result;

    if (type == 'h') {
      final h = _cachedHeights[key];

      if (h != null) {
        result = h;
      } else {
        result = _calculateDimension(type, value);
        _cachedHeights[key] = result;
      }

    } else if (type == 'w') {
      final w = _cachedWidths[key];

      if (w != null) {
        result = w;
      } else {
        result = _calculateDimension(type, value);
        _cachedWidths[key] = result;
      }
    }

    return result;
  }

  double _calculateDimension(String type, double percentage) {

    type = type;

    percentage = percentage * 0.01;

    double result = 0;
    if (type == 'w') {
      
      result = percentage * _screenWidth;

    } else if (type == 'h') {

      result = percentage * _screenHeight;
    }

    return result;
  }


  _verifyDDValue(double value) {

    if (value < 0) {
      throw InvalidPercentageException(percentage: value);
    } 

  }

  _verifyDDType(String type) {

    if (type != 'w' && type != 'h') {
      throw InvalidMeasureTypeException(type: type);
    }

  }
}
