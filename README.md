# Style Guide

## The purpose:

The purpose of Style Guide in Flutter is to create and consolidate a pattern for all the visual properties in the application, like colors, font styles, button properties, and many others.

The style guide is a structure composed by other structures, like a tree. A style guide has multiples Style Sets.

A Style Set is a set that contains multiple styles. It can be used inside the style guide, or by itself. One example of usage inside the style guide is to define the application buttons, which will have multiple styles for one button, like color, height, etc. One example of usage by itself, is to define global styles, like color palette, spaces between components, et cetera.

A Style is a set of properties, for example, the style 'color-palette' will have the properties 'primary', 'secondary', et cetera.

Properties are the atomics elements of the Style Guide. A property can be anything that can be used as property, like a Color, a Text, a number, etc.

#
## Defining a Style Guide:

To define a Style Guide, you need to provide a BuildContext, a Map and a bool (which will indicate if it will run in the debug mode, as default, it uses 'true').

The Map will represent the Style Guide as a structure, where the keys are the names of the Style Sets, and the values are Style Sets also represented by Maps (which we will call 'second layer').

The Second Layer map keys are the names of the Styles, and the values are the Styles, also represented by Maps (which we will call 'bottom layer').

The Bottom Layer map keys are the names of the Properties, and the Values are the Properties.

You can see an example below:

```dart

StyleGuide styleGuide = StyleGuide.init(
    context,
    {
        'global' : {
            'color-palette' : {
                'primary' : Colors.blue,
                'secondary' : Colors.white,
                'success' : Colors.green,
                'failure' : Colors.red,
                'font-color-white' : Colors.white,
                'font-color-black' : Colors.black,
            },
        },
        'typography' : {
            'font-sizes' : {
                'h1' : 30, // 30 pixels
                'h2' : 23, // 23 pixels
            },
        },
    }, 
    debug: true
)

```

#
## Using the Style Guide:

To get a Property from the Style Guide, you need to do:

```dart
styleGuide.get('style-set-key > style-key : property-key');
```
Example:

```dart
final Color backgroundColor = styleGuide.get('global > color-palette : primary'); // Colors.blue
```


# Screen Data

To create a responsive application that will work fine in any size of screen, you can instead of defining the measures in pixels, define it as a percentage of the screen size. With this purpose, we have the Screen Data class. 

The class Screen Data is an auxiliary tool to the Style Guide. It is used to calculate percentages of the screen measures. 

To use it, first, you just need to instanciate it like this, providing a BuildContext:

```dart
ScreenData screenData = ScreenData.init(context);
```

Then, you need to provide a DimensionData (which needs a value, that will represent the percetage of the screen you want, and a string 'w' or 'h', depending on which measure you want a percentage of, width or height) to the method 'dim()', as you can see below:

```dart
DimensionData dimensionData = DimensionData(25, 'w'); // 25% of the width
double result = screenData.dim(dimensionData);
```
Or just:

```dart
double result = screenData.dim(DimensionData(25, 'w')); // 25% of the width
```

You can also create a ScreenDimension by doing this:

```dart
DimensionData dimensionData = dd(25, 'w'); // 25% of the width
```

And get a percentage of the screen like this:

```dart
double result = screenData.dim(dd(25, 'w')); // 25% of the width
```

#
## Defining a Style Guide using Screen Data

Example:

```dart

StyleGuide styleGuide = StyleGuide.init(
    context,
    {
        'global' : {
            'color-palette' : {
                'primary' : Colors.blue,
                'secondary' : Colors.white,
                'success' : Colors.green,
                'failure' : Colors.red,
                'font-color-white' : Colors.white,
                'font-color-black' : Colors.black,
            },
        },
        'typography' : {
            'font-sizes' : {
                'h1' : dd(4,'w'), // 4% of the screen Width
                'h2' : dd(3,'w'), // 3% of the screen Width
            },
        },
    }, 
    debug: true
)

```

## Using a Style Guide with Screen Data
Example:

```dart
final double h1 = screenData.dim(styleGuide.get('typography > font-sizes : h1')); // 4% of the screen Width
```
#
## Full example of usage

```dart
import 'package:flutter/material.dart';
import 'package:style_guide/export.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  StyleGuide styleGuide;
  @override
  Widget build(BuildContext context) {
    styleGuide = StyleGuide.init(
      context,
      {
        'global': {
          'background-colors': {
            'primary': Colors.blue,
            'secondary': Colors.yellow,
            'success': Colors.red,
            'warning': Colors.amber,
            'error': Colors.red,
          },
        },
        'typography': {
          'title': {
            'style': FontStyle.normal,
            'color-primary': Colors.black,
            'color-secondary': Colors.white,
            'font-family': 'Raleway',
          },
          'subtitle': {
            'style': FontStyle.italic,
            'color-primary': Colors.grey,
            'font-family': 'Raleway',
          },
          'regular': {
            'style': FontStyle.normal,
            'color-primary': Colors.black,
            'color-secondary': Colors.white,
            'color-extra': Colors.grey,
            'font-family': 'RobotoMono',
          }
        },
        'spacing': {
          'margin': {
            'all-smaller': dd(1, 'h'),
            'right-smaller': dd(1, 'w'),
            'left-smaller': dd(1, 'w'),
            'top-smaller': dd(1, 'h'),
            'bottom-smaller': dd(1, 'h'),
            'horizontal-smaller': dd(0.5, 'h'),
            'vertical-smaller': dd(0.75, 'h'),
            'all-regular': dd(2, 'h'),
            'right-regular': dd(2, 'w'),
            'left-regular': dd(2, 'w'),
            'top-regular': dd(2, 'h'),
            'bottom-regular': dd(2, 'h'),
            'horizontal-regular': dd(1, 'h'),
            'vertical-regular': dd(1.2, 'h'),
            'all-large': dd(2.5, 'h'),
            'right-large': dd(2.5, 'w'),
            'left-large': dd(2.5, 'w'),
            'top-large': dd(2.5, 'h'),
            'bottom-large': dd(2.5, 'h'),
            'horizontal-large': dd(20.5, 'h'),
            'vertical-large': dd(4.85, 'h'),
          },
          'padding': {
            'all-smaller': dd(1, 'h'),
            'right-smaller': dd(1, 'w'),
            'left-smaller': dd(1, 'w'),
            'top-smaller': dd(1, 'h'),
            'bottom-smaller': dd(1, 'h'),
            'horizontal-smaller': dd(0.5, 'h'),
            'vertical-smaller': dd(0.75, 'h'),
            'all-regular': dd(2, 'h'),
            'right-regular': dd(2, 'w'),
            'left-regular': dd(2, 'w'),
            'top-regular': dd(2, 'h'),
            'bottom-regular': dd(2, 'h'),
            'horizontal-regular': dd(1, 'h'),
            'vertical-regular': dd(1.2, 'h'),
            'all-large': dd(2.5, 'h'),
            'right-large': dd(2.5, 'w'),
            'left-large': dd(2.5, 'w'),
            'top-large': dd(2.5, 'h'),
            'bottom-large': dd(2.5, 'h'),
            'horizontal-large': dd(1.5, 'h'),
            'vertical-large': dd(1.85, 'h'),
          },
          'border': {
            'all-smaller': dd(1, 'h'),
            'right-smaller': dd(1, 'w'),
            'left-smaller': dd(1, 'w'),
            'top-smaller': dd(1, 'h'),
            'bottom-smaller': dd(1, 'h'),
            'horizontal-smaller': dd(0.5, 'h'),
            'vertical-smaller': dd(0.75, 'h'),
            'all-regular': dd(2, 'h'),
            'right-regular': dd(2, 'w'),
            'left-regular': dd(2, 'w'),
            'top-regular': dd(2, 'h'),
            'bottom-regular': dd(2, 'h'),
            'horizontal-regular': dd(1, 'h'),
            'vertical-regular': dd(1.2, 'h'),
            'all-large': dd(2.5, 'h'),
            'right-large': dd(2.5, 'w'),
            'left-large': dd(2.5, 'w'),
            'top-large': dd(2.5, 'h'),
            'bottom-large': dd(2.5, 'h'),
            'horizontal-large': dd(1.5, 'h'),
            'vertical-large': dd(1.85, 'h'),
          },
        }
      },
    );
    return Scaffold(
      appBar: AppBar(
        backgroundColor: styleGuide.get('global > background-colors : success'),
        title: Text(
          'First Page',
          style: TextStyle(
            color: styleGuide.get('typography > title : color-primary'),
            fontStyle: styleGuide.get('typography > title : style'),
          ),
        ),
      ),
      body: Center(
        child: Container(
          child: SizedBox(
            height: styleGuide.dim(
              styleGuide.get('spacing > margin : vertical-large'),
            ),
            width: styleGuide.dim(
              styleGuide.get('spacing > margin : horizontal-large'),
            ),
            child: RaisedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SecondPage(
                      styleGuide: styleGuide,
                    ),
                  ),
                );
              },
              child: Icon(Icons.next_plan),
            ),
          ),
        ),
      ),
    );
  }
}

class SecondPage extends StatelessWidget {
  final StyleGuide styleGuide;

  const SecondPage({Key key, @required this.styleGuide}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Second Page',
          style: TextStyle(
            fontStyle: styleGuide.get('typography > subtitle : style'),
            color: styleGuide.get('typography > subtitle : color-primary'),
          ),
        ),
      ),
      body: Column(
        children: [
          Text(
            '''
          lorem ipsum dolor sit amet, consectetur adip
          lorem ipsum dolor sit amet, consectetur adip
          lorem ipsum dolor sit amet, consectetur adip
          lorem ipsum dolor sit amet, consectetur adip
          lorem ipsum dolor sit amet, consectetur adip
          lorem ipsum dolor sit amet, consectetur adip
          lorem ipsum dolor sit amet, consectetur adip
          lorem ipsum dolor sit amet, consectetur 
          lorem ipsum dolor sit amet, consectetur adip
          lorem ipsum dolor sit amet, consectetur adip
          ''',
            style: TextStyle(
              color: styleGuide.get('typography > regular : color-extra'),
              fontStyle: styleGuide.get(
                'typography > regular : style',
              ),
            ),
          ),
          Container(
            color: styleGuide.get('global >  background-colors : error'),
          ),
        ],
      ),
    );
  }
}

```